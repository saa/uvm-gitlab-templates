# UVM Gitlab Templates

Templates for use in projects (CI templates, Dockerfiles, etc)

See https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html
for more info on how to use this repository.

If you need help with these templates, please contact saa@uvm.edu
